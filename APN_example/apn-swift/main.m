//
//  main.m
//  apn-swift
//
//  Created by hajunho on 2016. 11. 20..
//  Copyright © 2016년 hajunho. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
